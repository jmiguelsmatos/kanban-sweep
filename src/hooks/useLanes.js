import { useEffect, useState } from "react";
import { lanes as DEFAULT_LANES } from "../constants/defaultLanes";

export const useLanes = () => {
  const [lanes, setLanes] = useState(
    JSON.parse(localStorage.getItem("lanes")) || DEFAULT_LANES
  );
  const [lane, setLane] = useState("");

  useEffect(() => {
    localStorage.setItem("lanes", JSON.stringify(lanes));
  }, [lanes]);

  const addLane = () => {
    setLanes([...lanes, lane]);
  };

  const removeLane = (index) => {
    lanes.splice(index, 1);

    setLanes([...lanes]);
  };

  return { lanes, addLane, setLane, removeLane };
};
