import { useCallback, useEffect, useState } from "react";

export const useTasks = () => {
  const [tasks, setTasks] = useState(
    JSON.parse(localStorage.getItem("tasks")) || []
  );
  const [task, setTask] = useState("");
  const [filteredTasks, setFilteredTasks] = useState([]);

  useEffect(() => {
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }, [tasks]);

  const changeStatus = (item, index) => {
    const task = tasks.filter((obj) => obj.name === item.name);
    task[0].status = index;

    setTasks([...tasks]);
  };

  const addTask = (data) => {
    tasks.push({ ...data });
    setTasks([...tasks]);
  };

  const deleteTask = (data) => {
    const taskIndex = tasks.indexOf(data);
    tasks.splice(taskIndex, 1);

    setTasks([...tasks]);
  };

  const editTask = (newTask, oldTask) => {
    const taskIdx = tasks.indexOf(oldTask);
    tasks[taskIdx] = newTask;

    setTasks([...tasks]);
  };

  const filterTasks = useCallback(
    (search) => {
      if (!search) {
        setFilteredTasks([...tasks]);
      } else {
        setFilteredTasks(tasks.filter((item) => item.tag?.includes(search)));
      }
    },
    [tasks]
  );

  return {
    tasks,
    task,
    filteredTasks,
    changeStatus,
    addTask,
    setTask,
    deleteTask,
    editTask,
    filterTasks,
  };
};
