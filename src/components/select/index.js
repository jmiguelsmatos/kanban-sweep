import { Select as MuiSelect, InputLabel, FormControl } from "@mui/material";
import { Controller } from "react-hook-form";
import PropTypes from "prop-types";

const Select = ({ name, label, control, defaultValue, children }) => {
  const labelId = `${name}-label`;
  return (
    <FormControl fullWidth>
      <InputLabel id={labelId}>{label}</InputLabel>
      <Controller
        render={({ field }) => (
          <MuiSelect labelId={labelId} label={label} {...field}>
            {children}
          </MuiSelect>
        )}
        name={name}
        control={control}
        defaultValue={defaultValue}
      />
    </FormControl>
  );
};

Select.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  control: PropTypes.object,
  children: PropTypes.node,
  defaultValue: PropTypes.number.isRequired,
};

export default Select;
