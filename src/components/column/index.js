import { useDrop } from "react-dnd";
import { ItemTypes } from "../../constants/itemTypes";
import { Box, Typography } from "@mui/material";
import PropTypes from "prop-types";

const Column = ({ children, index, name, dropTriggered, removeColumn }) => {
  const [{ isOver }, drop] = useDrop(
    () => ({
      accept: ItemTypes.CARD,
      drop: (item) => {
        dropTriggered(item, index);
      },
      collect: (monitor) => ({
        isOver: !!monitor.isOver(),
      }),
    }),
    [index]
  );
  return (
    <div
      ref={drop}
      style={{
        width: "20%",
        height: "80vh",
        padding: "10px",
        margin: "2px",
        background: "#B3E5FC",
        position: "relative",
      }}
    >
      <Box display="flex" justifyContent="space-between">
        <Typography>{name}</Typography>
        <Box onClick={() => removeColumn && removeColumn()}> X </Box>
      </Box>
      {children}
      {isOver && (
        <div
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            height: "100%",
            width: "100%",
            zIndex: 1,
            opacity: 0.5,
            backgroundColor: "#E1F5FE",
          }}
        />
      )}
    </div>
  );
};

Column.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number,
  name: PropTypes.string.isRequired,
  dropTriggered: PropTypes.func,
  removeColumn: PropTypes.func,
};

export default Column;
