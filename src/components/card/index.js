import { useDrag } from "react-dnd";
import { ItemTypes } from "../../constants/itemTypes";
import {
  Card as MuiCard,
  CardContent as MuiCardContent,
  Chip,
  Typography,
  Box,
} from "@mui/material";
import PropTypes from "prop-types";

const Card = ({ name, status, tag, onClick }) => {
  const [collected, drag] = useDrag(() => ({
    type: ItemTypes.CARD,
    item: { status, name },
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging(),
    }),
  }));

  return (
    <MuiCard
      ref={drag}
      style={{
        margin: "4px",
        cursor: collected.isDragging ? "resize" : "",
      }}
      raised
      onClick={() => onClick && onClick()}
    >
      <MuiCardContent>
        <Box display="flex" justifyContent="space-between">
          <Typography gutterBottom>{name}</Typography>
          {tag && <Chip label={tag} />}
        </Box>
      </MuiCardContent>
    </MuiCard>
  );
};

Card.propTypes = {
  name: PropTypes.string.isRequired,
  status: PropTypes.number.isRequired,
  tag: PropTypes.string,
  onClick: PropTypes.func,
};

export default Card;
