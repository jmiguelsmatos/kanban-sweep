import {
  Button,
  TextField,
  DialogActions,
  DialogContent,
  MenuItem,
  FormControl,
  Box,
} from "@mui/material";
import { useContext } from "react";
import { useForm } from "react-hook-form";
import { LanesContext } from "../../containers/kanbanBoard";
import Select from "../../components/select";
import PropTypes from "prop-types";

const TaskForm = ({ submit, isEditing, onDelete, defaultValues }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
  } = useForm({
    defaultValues: {
      name: "",
      status: 0,
      ...defaultValues,
    },
  });

  const { lanes } = useContext(LanesContext);

  const onSubmit = (data) => submit(data);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <DialogContent>
        <Box display="flex" flexDirection="column">
          <Box m={2}>
            <FormControl>
              <TextField
                label="Name"
                {...register("name", { required: true })}
                error={errors.name}
                helperText={errors.name && "field required"}
                fullWidth
              />
            </FormControl>
          </Box>
          <Box m={2}>
            <FormControl>
              <TextField
                label="Tag"
                {...register("tag")}
                error={errors.tag}
                fullWidth
              />
            </FormControl>
          </Box>
          <Box m={2}>
            <Select
              id="trinity-select"
              name="status"
              label="Status"
              control={control}
              fullWidth
            >
              {lanes.map((lane, idx) => (
                <MenuItem key={lane} value={idx}>
                  {lane}
                </MenuItem>
              ))}
            </Select>
          </Box>
        </Box>
      </DialogContent>
      <DialogActions>
        {isEditing && (
          <Button
            type="button"
            variant="contained"
            color="error"
            onClick={() => onDelete && onDelete()}
          >
            Delete
          </Button>
        )}
        <Button type="submit" variant="contained">
          Save
        </Button>
      </DialogActions>
    </form>
  );
};

TaskForm.propTypes = {
  isEditing: PropTypes.bool,
  onDelete: PropTypes.func,
  submit: PropTypes.func,
  defaultValues: PropTypes.shape({
    name: PropTypes.string,
    status: PropTypes.number,
    tag: PropTypes.string,
  }),
};

export default TaskForm;
