import Card from "../../components/card";
import Column from "../../components/column";
import TaskForm from "../../forms/task";
import { useTasks } from "../../hooks/useTasks";
import { useLanes } from "../../hooks/useLanes";
import {
  Box,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
} from "@mui/material";
import { createContext, useEffect, useState } from "react";

export const LanesContext = createContext(null);

const KanbanBoard = () => {
  const {
    tasks,
    filteredTasks,
    changeStatus,
    addTask,
    deleteTask,
    editTask,
    filterTasks,
  } = useTasks();
  const { lanes, addLane, setLane, removeLane } = useLanes();

  const canCreateTask = lanes && lanes.length > 0;

  const [openDialogLane, setOpenDialogLane] = useState(false);
  const [editingTask, setEditingTask] = useState({});
  const [openDialogTask, setDialogTask] = useState(false);

  const [searchValue, setSearchValue] = useState("");

  const isEditing = Object.entries(editingTask).length > 0;

  const handleClose = () => {
    setOpenDialogLane(false);
    setDialogTask(false);
    setEditingTask({});
  };

  useEffect(() => {
    filterTasks(searchValue);
  }, [searchValue, tasks, filterTasks]);

  return (
    <LanesContext.Provider value={{ lanes }}>
      <Box>
        <Box display="flex" justifyContent="space-between">
          <TextField
            label="Filter by tag"
            defaultValue={searchValue}
            disabled={!canCreateTask}
            onChange={(e) => setSearchValue(e.target.value)}
            size="small"
          />
          <Box display="flex">
            <Box m={1}>
              <Button
                variant="contained"
                color="secondary"
                onClick={() => setOpenDialogLane(true)}
                m={1}
              >
                New swim lane
              </Button>
            </Box>
            <Box m={1}>
              <Button
                variant="contained"
                disabled={!canCreateTask}
                onClick={() => setDialogTask(true)}
              >
                Create task
              </Button>
            </Box>
          </Box>
        </Box>

        <Box display="flex">
          {lanes.map((lane, index) => {
            return (
              <Column
                key={lane}
                name={lane}
                index={index}
                dropTriggered={(item, index) => changeStatus(item, index)}
                removeColumn={() => removeLane(index)}
              >
                {filteredTasks
                  .filter((card) => card.status === index)
                  .map((item) => (
                    <Card
                      key={item.name}
                      name={item.name}
                      status={item.status}
                      tag={item.tag}
                      onClick={() => {
                        setEditingTask(item);
                        setDialogTask(true);
                      }}
                    />
                  ))}
              </Column>
            );
          })}

          <Dialog onClose={handleClose} open={openDialogLane}>
            <DialogTitle id="alert-dialog-title">
              Create new swiming lane
            </DialogTitle>
            <DialogContent>
              <TextField
                onChange={(e) => setLane(e.target.value)}
                fullWidth
              ></TextField>
            </DialogContent>
            <DialogActions>
              <Button
                variant="contained"
                onClick={() => {
                  addLane();
                  handleClose();
                }}
              >
                Save
              </Button>
            </DialogActions>
          </Dialog>

          <Dialog onClose={handleClose} open={openDialogTask}>
            <Box>
              <DialogTitle>{isEditing ? "Edit" : "New"} task</DialogTitle>
              <TaskForm
                submit={(data) => {
                  isEditing ? editTask(data, editingTask) : addTask(data);
                  handleClose();
                }}
                defaultValues={editingTask}
                isEditing={isEditing}
                onDelete={() => {
                  deleteTask(editingTask);
                  handleClose();
                }}
              />
            </Box>
          </Dialog>
        </Box>
      </Box>
    </LanesContext.Provider>
  );
};

export default KanbanBoard;
