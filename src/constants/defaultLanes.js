const TO_DO = "To do";
const IN_PROGRESS = "In Progress";

export const lanes = Object.freeze([TO_DO, IN_PROGRESS]);
